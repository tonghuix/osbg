[![Build Status](https://semaphoreci.com/api/v1/tonghuix/osbg/branches/master/badge.svg)](https://semaphoreci.com/tonghuix/osbg)
[![Build Status](https://drone.io/bitbucket.org/tonghuix/osbg/status.png)](https://drone.io/bitbucket.org/tonghuix/osbg/latest)


# 开源社群入门指南
 (Beginner Guide for Open Source Community) 

下载地址：[点此下载](https://tonghuix.fedorapeople.org/pub/osbg.pdf)

最新构建版：[点此下载](https://drone.io/bitbucket.org/tonghuix/osbg/files/osbg.pdf)

## 配置写作环境

需要 Git 和 [Markdown](https://help.github.com/articles/github-flavored-markdown) 基础。

详情参阅：[BUILD.md](BUILD.md)

此项目基于 [蔡煜 Larry Cai](http://larrycaiyu.com/) 的 [sdcamp](https://github.com/larrycai/sdcamp/) 项目框架修改而来，感谢他的努力，使得书写技术书籍变得如此简单。

## 参与编写须知 


## 著作权及版权声明 

本书基于 GNU 自由文档协议 1.3 版 (GNU Free Documentation License 1.3) 授权发布，可自由复制和分发，和/或基于 GNU 自由文档协议 1.3 版或自由软件基金会发布的更高版本，做出修改。本书使用 Markdown 书写，源码地址：<https://github.com/tonghuix/osbg>

![](http://www.gnu.org/graphics/heckert_gnu.small.png)
[GNU 自由文档协议 1.3 版（GNU Free Documentation License version 1.3）](http://www.gnu.org/licenses/fdl.html)



