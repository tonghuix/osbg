sudo apt-get -y install texlive-xetex
sudo apt-get -y install texlive-latex-recommended # main packages
sudo apt-get -y install texlive-latex-extra # package titlesec
sudo apt-get -y install ttf-wqy-microhei ttf-wqy-zenhei # from WenQuanYi
sudo apt-get -y install texlive-fonts-recommended
sudo apt-get -y install latex-cjk-all  fonts-liberation ttf-liberation 
sudo apt-get -y install fonts-arphic-gkai00mp fonts-arphic-ukai latex-cjk-chinese-arphic-gkai00mp #install Kaiti font
sudo apt-get -y install fonts-arphic-gbsn00lp latex-cjk-chinese-arphic-gbsn00lp 
sudo fc-cache
